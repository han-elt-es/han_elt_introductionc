/* Use of an int array */

#include <stdio.h>

#define SIZE_data1 4
#define SIZE_data2 10

int main(void)
{
   int data1[SIZE_data1] = ....;
   int data2[SIZE_data2] = ....;
   int sum = 0;
   int input;
   int j = 0;

   /* Enter all elements in data1 array */
   /* first print an appropiate message to ask for input */

   for (int i = 0; i < SIZE_data1; i++)
   {
      scanf("....", &data1[i]);
      /* Remove al buffered char in stdin */
      while (getchar() != '\n')
      {
         /* Empty loop */
      }
   }


   /* Calculate sum of all elements in data1 */
   for (....)
   {
      ....
   }

   /* print the result of the sum*/

   /* calculations with pow() */

   for (...)
   {
      data2[i] = pow(i, 4);
      printf("%d\n", data2[i]);
   }

   /* find the first value between 10 and 70 */

   while ()          // find the correct logical expression (condition) 
   {
      .....          // go to the next value in data2
   }

   printf("The first value between 10 and 70 in data 2 is: %d", data2[....]);
   
   return 0;
}
