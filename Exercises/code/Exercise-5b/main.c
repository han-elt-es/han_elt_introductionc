// Exercise: the use of functions

#include <stdio.h>

/* Insert the function prototype, CtoF()  */
double ...............

int main(void)
{
   /* Define a start variable (double) for celsius to be used in the table */
   /* Define a end variable (double) for celsius to be used in the table */
   /* Define a interval variable (double) for celsius to be used in the table */

   ....

   /* ask the user to enter a start value in Celsius */
   printf("Please enter a start temperature in degrees Celsius:");
   .......
   /* ask the user to enter a start value in Celsius */
   printf("Please enter a end temperature in degrees Celsius:");
   .......
   /* ask the user to enter a start value in Celsius */
   printf("Please enter a interval for your table:");
   .......

   /* Create the table header */
   printf("...\t...");

   /* create a loop to print the table */

   while (...)
   {
     printf("%.2lf\t%.2lf\n", ...., .....);
      .....
   }

   return 0;
}


/* Complete CtoF function */
double CtoF(double celsius)
{
   return celsius * 9.0/5.0 + 32;
}
