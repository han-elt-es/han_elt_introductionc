TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.c \
        postalcode.c

HEADERS += \
   postalcode.h

DISTFILES += \
   out.txt
