#ifndef POSTALCODE_H
#define POSTALCODE_H

#define NO_POSTCODES 8
// Postalcode structure
typedef struct
{
   int number;
   char twochars[3];
} postalcode_t;

//add search, read and write functions

//int writePostalCodes(postalcode_t pcs[], int size, const char filename[]);

#endif
