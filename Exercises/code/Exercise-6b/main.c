#include <stdio.h>
#include <string.h>
#include "postalcode.h"

// int readPostalCodes(postalcode_t pcs[], int size, const char filename[]);
int writePostalCodes(postalcode_t pcs[], int size, const char filename[]);
int findPostalcode(postalcode_t postalcodes[], postalcode_t searchcode);

int main(void)
{
   postalcode_t postalcodes[NO_POSTCODES] = {
      {1234, "AB"},
      {5657, "TT"},
      {8765, "YY"},
      {5644, "TT"},
      {1234, "AB"},
      {5643, "TT"},
      {8754, "YY"},
      {5623, "TT"},
   };
   postalcode_t searchcode = {8765, "YY"};
   int result = 0;

   result  = findPostalcode(postalcodes, searchcode);
   printf("Postalcode found at index: %d\n", result);
   // read write
   char fileName[40] = "postalCodes.txt";

   writePostalCodes(postalcodes, NO_POSTCODES, fileName);

   postalcode_t readCodes[NO_POSTCODES];

   /*
   FILE *dataFile;
   // Read postalcodes
   dataFile = fopen(fileName, "r");
   if (dataFile != NULL)
   {
      int i = 0;
      while(!feof(dataFile) && i < NO_POSTCODES)
      {
//         fscanf(.....);
         i++;
      }
      result = i;
   }
   else
   {
      result = -1;
   }
   fclose(dataFile);
*/

   for (int i = 0; i < NO_POSTCODES; i++)
   {
      printf("Read: %d %s\n", readCodes[i].number, readCodes[i].twochars);
   }

   return 0;
}

// find a postalcode in an array of postalcode_t's
int findPostalcode(postalcode_t postalcodes[], postalcode_t searchcode)
{
   int result = 0;
   for(int i = 0; i < NO_POSTCODES; i++)
   {
      if ((postalcodes[i].number == searchcode.number) &&
          (strcmp(postalcodes[i].twochars, searchcode.twochars) == 0))
      {
         result = i;
         break;
      }
      result = -1;
   }
   return result;
}

int writePostalCodes(postalcode_t pcs[], int size, const char filename[])
{
   FILE *dataFile = fopen(filename, "w");
   int recordsWritten = 0;
   // Write postalcodes
   if (dataFile != NULL)
   {
      for (int i = 0; i < size; i++)
      {
         fprintf(..., "%d %s\n", ..., .....);
         printf(".");               // show that somethings happening
         recordsWritten++;
      }
   }
   else
   {
      printf("Error cannot open file\n");
   }
   printf("\n");
   fclose(dataFile);
   return recordsWritten;
}


