// Exercise: For loop to While loop and vice versa.

#include <stdio.h>

int main(void)
{
   /* for loop to while loop */
   for (int i = 0; i < 5; i++)
   {
      printf("Index in the for loop i is now: %d\n", i);
   }

   // Insert the while loop here

   printf("The output of the while loop:\n");

   int i = 0;
   while (.....)
   {
      printf("Index in the while loop i is now: %d\n", i);
      ......
   }

   /* while loop to for loop */

   printf("The output of the while loop:\n");

   int j = 10;
   while (j > 0)
   {
      printf("Index in the while loop j is now: %d\n", j);
      j--;
   }

   // Insert the for loop here

   printf("The output of the for loop:\n");

   for (.........)

   {
      printf("Index in the for loop j is now: %d\n", j);
   }

   return 0;
}
