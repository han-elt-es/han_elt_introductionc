#include <stdio.h>

/// \author John van den Hooven
///
/// This program use C expressions to do some math with Ohm's law.
///
/// The purpose of the program is to calculate Voltage, Resistance and Current.
/// using different values of Voltage, Resistance and Current.  
/// Ohms law says: Voltage = Current * Resistance (van = I * R)
/// The formula to calculate current: I = V / R
/// The formula to calculate resistance: R = V / I
///
/// Fill the .... parts with statements, functions or expressions
///
/// Exercise:
/// 1) Given the values of I and R:
///    a) Calculate V
///    b) Print all values V, I and R
/// 2) Increase the value of R with 5000
///    a) Calculate V
///    b) Print all values V, I, and R
/// 3) Change values V and R (V = 1.5, R = 1000.0)
///    a) Calculate I
///    b) Print all values V, I, and R
/// 4) Make I three times larger and add 5, V remains the same value.
///    (use for I the result of calculation 3) )
///    a) Calculate R
///    b) Print all values V, I, and R
/// 5) Use one printf() invocation to print all values V, I and R again
///

int main(void)
{
   double V = 0.0;
   double I = 10.0;
   double R = 20000.0;

   /* Calculate V */
   V = I * R; 

   /* Print all values: V, I and R */
   printf("V = %.1lf V\n", V);
   printf("I = %.1lf A\n", I);
   printf("R = %.1lf Ohm\n\n", R);

   /* Increase the resistance R by 5000 Ohm,
    * I remains the same value */
   R = .... + 5000;
   /* Recalculate V, use the formula */
   V = ....;

   /* Print all values: V, I and R */
   printf("V = %.1lf V\n", V);
   printf("I = %.1lf A\n", I);
   printf("R = %.1lf Ohm\n\n", R);

   /* Change values V and R */
   V = 1.5;
   R = 1000.0;
   /* Calculate I */
   I = ....;

   /* Print all values: V, I and R */
   ....;
   ....;
   ....;

   /* Make I three times larger and add 5,
    * V remains the same value */ 
   I = ....;
   /* Calculate R */
   R = ....;
   
   /* Use one printf() invocation to print all values V, I and R again */
   ....;

   return 0;
}
