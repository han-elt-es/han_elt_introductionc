// Exercise: the use of structs and arrays of structs

#include <stdio.h>

typedef struct {
   int number;
   char twoChars[3];
} postalcode_t;

#define NUMBER_OF_PCS 3

int main(void)
{
   /* extend the code .... to completely initialize
      the array with information */
   postalcode_t pcs1[NUMBER_OF_PCS] = {
      {1234, "AB"}, ....};

   /* Array initialised with all 0's */
   postalcode_t pcs2[NUMBER_OF_PCS] = {0};

   /* Print the size of the struct in bytes */
   /* use appropriate function */
   printf(....);

   /* Print the contents of the complete pcs1 array */
   /* add the condition in the for loop             */
   /* add the code to print the information at .... */
   printf("Number\ttwoChars\n");
   for (int i = 0; i < .....; i++)
   {
      printf("%d\t%s\n", ...., .....);
   }

   /* Compare pcs1[0] and pcs1[1] and print appropriate texts */
   /* Hint: you can't not compare two structs with "pcs1[0] == pcs1[1]" */
   if (....)
   {
      printf(....);
   }
   else
   {
      printf(....);
   }

   /* Compare pcs1[0] and pcs1[2] and print appropriate texts */
   if (....)
   {
      ....
   }
   else
   {
      ....
   }

   /* Create a loop to copy the contents of pcs1 to pcs2 */
   /* Hint: you can't copy information by using pcs1[0] = pcs2[0] */

   for (int i = 0; i < ...; i++)
   {
      ....
   }

   /* Create a loop to show the result of the copy */

   printf("pcs1\t\tpcs2\n")

   for (int i = 0; i < ...; i++)
   {
      printf("%d\t%s\t%d\t%s\n", .....)
   }

   return 0;
}
