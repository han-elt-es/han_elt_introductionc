#include <stdio.h>

int main(void)
{
   char inp = ' ';             // define a character and initialize with q

   printf("..........");     // add text to ask the user to enter w,z,a,s,q
   scanf(" %c", &inp); // Function to get a key from the keyboard

   switch (inp)
   {
      case 'w':
         printf("Up\n");
         break;
         ...                 // (1 .. 6) complete the switch case
         ...
         ...
      case ...:
         printf("Left\n");
         break;
      case 'q':
         printf("Quit\n");
         break;
      ....                   // (7) complete the switch for any other character
   }

   return 0;
}
