/// \author John van den Hooven
///
/// Exercise 1a:
/// 1) Compile and run this program that prints the text
///    "Hello Word C Programming".
/// 2) In the first printf, replace in the string all the spaces
///    with a \\n. What is the effect?
/// 3) Add in the second printf function invocation your own text
///    that consists of different words separated by spaces.
/// 4) Replace in the second printf the spaces with a \t. What is the
///    effect?
/// 5) With the third printf, print an integral value 11 with
///    %d in a string and a floating point value 1.123 with %lf.
/// 6) Print the floating value 1.123 with 2 numbers behind the
///    decimal point as well.
/// 7) Copy the line created with the third printf but swap %d and
///    %lf without switching the numerical values.
///       a) Why is this code incorrect?
///       b) What is the output?
///       c) Does the compiler gives any errors or warnings?
///

#include <stdio.h>

int main(void)
{
   printf("Hello World C programming!\n");

   /* remove "//" in the following line when starting exercise 2) */
   // printf(....);

   /* remove "//" in the following line when starting exercise * 5) */
   // printf("Integer value = %d\nfloating point value = % lf\n ", ....);

   return 0;
}
