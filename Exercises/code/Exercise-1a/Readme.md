\section sec31 Exercise 1 Getting familiar with QtCreator and C programming

\subsection Exercise 1a The printf function and strings

Use the '1a' code from the repository. 

-# Compile and run the program that prints the text "hello world C programming" in a terminal.

-# Add a printf function to a new line with its own text that consists of differentwords separated by spaces.

-# In the first printf, replace in the string all the spaces with a \n. What is the effect? See C book, p. 34.

-# In the second printf, replace the spaces with a \\t. What is the effect?

-# Print a \ (backslash) at the end of the second string.

-# With printf, print an integral value 1 with %d in a string and a floating point value 1.123 with %f. See C book, p. 36.

-# Print the floating point value 1.123 with 2 numbers behind the decimal point as well. See C book, p. 37.

-# Copy the line created with printf but swap the %d and %f without switching the numerical values. Why is this code incorrect? What is the output? Does the compiler give any error or a warning?

\subsection sec312 Exercise 1b Ohm's law calculations

Create the Exercise1b C-project. Enter the code for the exercises below to the main.c of Exercise 1a. Ohm's Law is the main focus: U = I × R.

Use the '1b' code from gitlab. 

-# Copy next code into main.c and read the comments. These comments describe the code you have to implement.