#include <stdio.h>
#include "bytebitfunc.h"

#define NO_OF_BITS 8

// Bitwise macros
#define BIT_SET(var, bit) (var |= (1<<bit))
#define BIT_RESET(var, bit) (var &= ~(1<<bit))
#define BIT_TOGGLE(var, bit) (var ^= (1<<bit))
#define BIT_IS_SET(var, bit) (var & (1<<bit))
#define BIT_IS_CLEAR(var, bit) (!(var & (1<<bit)))

// Do not forget to uncomment the exercise lines

int main(void)
{
   // string to be used te represent a byte in bits
   char buffer[NO_OF_BITS + 1] = {0};
   // two variables to represent bytes
   unsigned char byte_a;
   unsigned char byte_b;
   byte_a = 213;
   byte_b = 213;

   ///set bit 5 of byte_a
   val2str(buffer, ...., sizeof(buffer));
   printf("6.1.3a) Before:\t%s\n", buffer);

//   byte_a |= ......;

   val2str(buffer, ...., sizeof(buffer));
   printf("6.1.3a) After:\t%s\n", buffer);

   ///reset bit 6 of byte_a
   val2str(buffer, ....., sizeof(buffer));
   printf("6.1.3b) Before:\t%s\n", buffer);

//   .... &= .....;        //add code

// val2str(....);
   printf("6.1.3b) After:\t%s\n", buffer);

   /// toggle bit 7 of byte_a
// val2str(.....));
   printf("6.1.3c) Before:\t%s\n", buffer);

   //   byte_a ......;        // complete code

// val2str(....);
   printf("6.1.3c) After:\t%s\n", buffer);

//Using the macros
//set bit 5 of byte_b
// val2str(....);
   printf("6.1.2a) Before:\t%s\n", buffer);

// BIT_SET(.......);

// val2str(....);
   printf("6.1.2a) After:\t%s\n", buffer);

//reset bit 6 of byte_b
// val2str(....);
   printf("6.1.2b) Before:\t%s\n", buffer);

// ......(byte_b, 6);    // add the macro

// val2str(....);
   printf("1b) After:\t%s\n", buffer);

//toggle bit 7 of byte_b
// val2str(....);
   printf("1c) Before:\t%s\n", buffer);

   //  ...........;           // add the macor

// val2str(....);
   printf("1c) After:\t%s\n", buffer);

//Check the setting of bit 3 using code
//   if (......)           // add the correct (bit wise) logical expression
//   {
//      printf("3a) Bit 3 of byte_b is set\n");
//   }
//   else
//   {
//      printf("3a) Bit 3 of byte_b is not set\n");
//   }

//Check the setting of bit 4 using macro
//   if (......)  // add the correct macro
//   {
//      printf("3b) Bit 4 of byte_b is set\n");
//   }
//   else
//   {
//      printf("3b) Bit 4 of byte_b is not set\n");
//   }
   return 0;
}

