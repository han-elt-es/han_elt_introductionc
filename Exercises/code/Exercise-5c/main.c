// Exercise: functions and references

#include <stdio.h>

/* Complete the function prototype that simulates the reading of two
 * sensors */
int readsensors(......);

int main(void)
{
   int rpm = 0;
   double speed = 0.0;
   int result = 0;

   printf("Sensor values before function call:\n");
   printf("rpm: %d\n", rpm);
   printf("speed: %.2lf\n\n", speed);

   /* Complete function call, use the reference to rpm and speed */
   result = readsensors(......);

   printf("Sensor values after function call:\n");
   printf("rpm: %d\n", rpm);
   printf("speed: %.2lf\n\n", speed);

   return 0;
}

/* Function that simulates the reading of two sensors */
int readsensors(........)
{
   /* simulate sensor information                     */
   /* assing the value of rpm and speed               */
   /* return sensor information and use dereferencing */

   /* use dereferencing to assign the values to rpm and speed */
   ....rpm = 10;
   ....speed = 12.5;

   return 0;
}
