// Exercise: Strings and character array's

#include <stdio.h>
#include <string.h>
#include ... // add the correct header file

#define SIZE 30

int main(void)
{
   /* String: char array */

   /* Add the text here to initialize the string text */
   char text[SIZE] = "..";
   int nDigits = 0; // define a variable to count the number of digits

   for (int i = 0; i < strlen(text); i++)
   {
      if (isdigit(text[i]))
      {
         ......; // Add the code to count the digits
      }
   }

   /* Print number of digits */
   printf("The string %s contains %d digits\n", text, nDigits);

   /* Change contents of text string array */
   strcpy(text, ......);

   printf("The string text is now: %s\n", text);

   /* Change all lower case characters to upper case */
   for (......) // Use a for loop to iterate through text
   {
      ...... = ......;
   }

   printf("The string text is after toupper(): %s\n", text);

   /* Print all characters and their hexadecimal value prefixed by 0x */
   for (int i = 0; i < strlen(text); i++)
   {
      printf("....\t.....", ..., ...);
   }

   return 0;
}
