// Exercise: functions

// Checkout the exercise book for the complete exercises 5.1
// this only a part of the framework!

#include <stdio.h>

/* Functions prototypes:                  */
/* Define the two functions               */
/* Use FtoC as an example for CtoF        */

double FtoC(double fahrenheit);
double ...(...);

int main(void)
{
   /* Define an input variable (double) for celsius */
   /* Define an input variable (double) for fahrenheit */
   double .....;
   double .....;

   /* Ask the user to enter a value in Celsius */
   printf("Please enter a start temperature in degrees Celsius:");
   scanf("...", ...);

   /* Make a call to the function CtoF and print the result */
   .... =  CtoF(....);
   printf("\n%.2lf degr. Celsius is %.2lf degr. Fahrenheit\n", ...., .....);

   return 0;
}

/* Functions implementations */

double FtoC(double fahrenheit)
{
   return (fahrenheit - 32) / (9 / 5);
}

/* Complete the CtoF function */
double CtoF(double celsius)
{
   return ......;
}
