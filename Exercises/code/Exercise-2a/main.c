/*
 * Fahrenheit and Celsius conversions
 * un-comment the lines to be the exercise
 */
#include <stdio.h>

int main(void)
{
   double Fahrenheit = 10.0;
   double Celsius = 0.0;

   // Celsius = ......;
   // printf("Fahrenheit = %lf\n", ......);
   // printf("Celsius = %lf\n", ......);

   // Use the formula to do the reverse calculation

   // Fahrenheit = ......;
   // printf("Fahrenheit = %lf\n", ......);
   // printf("Celsius = %lf\n", ......);

   // Repeat 3 times with different values

   // Calculate Fahrenheit temperature at 0 degrees Celsius
   // Print with 2 decimals

   // Fahrenheit = ......;
   // printf("Fahrenheit at 0 degree Celsius  = %lf\n", ......);

   // print Fahrenheit temperature with 10 decimals

   // printf("Fahrenheit at 0 degree Celsius  = %lf\n", ......);

   return 0;
}
