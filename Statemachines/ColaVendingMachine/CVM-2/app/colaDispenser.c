#include "colaDispenser.h"
#include "devConsole.h"

//--------------------------------------------------------------- CoLa Dispenser

void CLDinitialise(void)
{
   DCSdebugSystemInfo("Cola Dispenser: initialised");
}

void CLDdispenseCola(void)
{
   DCSdebugSystemInfo("Cola Dispenser: dispensed cola");
}
