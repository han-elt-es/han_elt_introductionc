#include "states.h"

const char *stateText(state_e state)
{
   switch (state)
   {
      case S_NO:
         return "S_NO";
      case S_START:
         return "S_START";
      case S_INITIALISE_SUBSYSTEMS:
         return "S_INITIALISE_SUBSYSTEMS";
      case S_CONFIGURE: 
         return "S_CONFIGURE";
      case S_WAIT_FOR_COINS:
         return "S_WAIT_FOR_COINS";
      case S_PROCESS_20C:
         return "S_PROCESS_20C";
      case S_PROCESS_50C:
         return "S_PROCESS_50C";
   }
   return "<state not defined>";
}