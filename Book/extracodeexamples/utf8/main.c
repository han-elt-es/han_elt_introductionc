#include <locale.h>
#include <stdio.h>
#include <string.h>
#include <wchar.h>

// Linux: ok
// Windows10: enable UTF8 (beta, experimental), only euro
int main(void)
{
   setlocale(LC_CTYPE, "");

   wchar_t star = 0x2605;
   wprintf(L"Hello UTF8 star: %lc\n", star);

   wchar_t ohm = 0x2126;
   wprintf(L"Hello UTF8 ohm:  %lc\n", ohm);

   wchar_t euro = 0x20AC;
   wprintf(L"Hello UTF8 euro: %lc\n", euro);

   return 0;
}
