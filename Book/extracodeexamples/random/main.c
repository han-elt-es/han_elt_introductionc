#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
   printf("Always same sequence:\n");
   for (int i = 0; i < 10; i++)
   {
      int dice = (rand() % 6) + 1;
      printf(" random dice = %d\n", dice);
   }
   printf("\n");

   printf("Sequence seeded by time:\n");
   srand(time(NULL));
   for (int i = 0; i < 10; i++)
   {
      int dice = (rand() % 6) + 1;
      printf(" random dice = %d\n", dice);
   }
   printf("\n");

   return 0;
}
