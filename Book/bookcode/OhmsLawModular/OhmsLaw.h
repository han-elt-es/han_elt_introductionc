#ifndef OHMSLAW_H
#define OHMSLAW_H

/* Prototype of the calculation functions */
double calcVoltage(double I, double R);
double calcCurrent(double U, double R);
double calcResistance(double U, double I);

#endif
