TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
   OhmsLaw.c \
   PrintFunctions.c \
   main.c

DISTFILES += \
   out.txt

HEADERS += \
   OhmsLaw.h \
   PrintFunctions.h
