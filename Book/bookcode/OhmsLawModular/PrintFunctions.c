#include "PrintFunctions.h"

#include <stdio.h>

void printAll(double I, const char Iname[], double R, const char Rname[],
              double U, const char Uname[])
{
   printf("%s = %.1lf A\n", Iname, I);
   printf("%s = %.1lf Ohm\n", Rname, R);
   printf("%s = %.1lf V\n\n", Uname, U);
}

void printAllSingleLine(double I, const char Iname[], double R,
                        const char Rname[], double U, const char Uname[])
{
   printf("%s = %.1lf A, %s = %.1lf Ohm, %s = %.1lf V\n\n", Iname, I,
          Rname, R, Uname, U);
}
