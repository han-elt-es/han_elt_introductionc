#ifndef PRINTFUNCTIONS_H
#define PRINTFUNCTIONS_H

/* Prototype of the print functions used in this program */
void printAll(double I, const char Iname[], double R, const char Rname[],
              double U, const char Uname[]);
void printAllSingleLine(double I, const char Iname[], double R,
                        const char Rname[], double U, const char Uname[]);

#endif
