#include <stdio.h>

/* Example of using logical expression in C */

int main(void)
{
   int a = 10;
   int b = 5;
   int c = 7;
   int result = 0;

   printf("a = %d  b = %d  c = %d\n\n", a, b, c);

   result = a > 2;
   printf("a > 2   %d\n", result);

   result = b < a;
   printf("b < a   %d\n", result);

   result = b == 5;
   printf("b == 5   %d\n", result);

   result = a != 10;
   printf("a != 10   %d\n", result);

   result = (a != 10) && (b == 5);
   printf("(a != 10) && (b == 5)   %d\n", result);

   result = (a != 10) || (b == 5);
   printf("(a != 10) || (b == 5)   %d\n", result);

   result = (a > c) && (b < c);
   printf("(a > c) && (b < c)   %d\n", result);

   result = (a == 3) && (b < c);
   printf("(a == 3) && (b < c)   %d\n", result);

   result = (a / b - a % b) > 0;
   printf("(a / b - a %% b) > 0   %d\n", result);

   return 0;
}
