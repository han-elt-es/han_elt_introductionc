Please enter a string of more than 5 characters:helloworld
Value stored in string str1: hellow
The following characters remain in the buffer:
o       111
r       114
l       108
d       100

Please enter a string of more than 5 characters:helloworld
Value stored in string str1: hellow
Please enter another string:Value stored in string str2: orld
                            ^
                            at this point nothing is entered
                            the second scanf() takes its input
                            from the keyboard buffer.

