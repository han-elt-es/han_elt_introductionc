// Demonstration of the contents of the keyboard buffer

#include <stdio.h>

int main(void)
{
   char str1[20] = "";
   char str2[20] = "";
   char cvalue = ' ';

   // First run scanf() puts 6 characters in string
   printf("Please enter a string of more than 5 characters:");
   scanf("%6s", str1);
   printf("Value stored in string str1: %s\n", str1);

   printf("The following characters remain in the buffer:\n");
   cvalue = (char)getchar();
   while (cvalue != EOF && cvalue != '\n')
   {
      printf("%c\t%d\n", cvalue, cvalue);
      cvalue = (char)getchar();
   }

   // Second run scanf puts 6 characters in str1
   // the remaining is put into str2 without waiting for keystrokes
   printf("Please enter a string of more than 5 characters:");
   scanf("%6s", str1);
   printf("Value stored in string str1: %s\n", str1);

   printf("Please enter another string:");
   scanf("%s", str2);
   printf("Value stored in string str2: %s\n", str2);

   return 0;
}
