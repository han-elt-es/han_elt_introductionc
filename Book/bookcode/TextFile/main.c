#include <stdio.h>

#define N_DATA 5

int main(void)
{
   double data[N_DATA] = {1.1, 2.2, 3.3, 4.4};

   FILE *dataFile = fopen("storeData.txt", "w");
   if (dataFile != NULL)
   {
      for (int i = 0; i < N_DATA; i++)
      {
         fprintf(dataFile, "%lf\n", data[i]);
         printf(" W data[%d] = %lf\n", i, data[i]);
      }
      puts("");
   }

   fclose(dataFile);

   dataFile = fopen("storeData.txt", "r");
   if (dataFile != NULL)
   {
      for (int i = 0; i < N_DATA; i++)
      {
         fscanf(dataFile, "%lf\n", &data[i]);
         printf(" R data[%d] = %lf\n", i, data[i]);
      }
      puts("");
   }
   fclose(dataFile);

   return 0;
}
