#include "sum.h"

#include <stdio.h>

/* A function definition with local and formal parameters */
int sum(int a, int b)
{
   int sumresult; // Local parameter in function sum

   printf("Value a: %d\n", a); // a and b are formal parameters
   printf("Value b: %d\n", b);

   sumresult = a + b;
   printf("Value result: %d\n", b);

   printf("Value result: %d\n", locsum);
   printf("Value result: %d\n", globsum);

   return sumresult;
}
