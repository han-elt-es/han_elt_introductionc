#include "sum.h"

#include <stdio.h>

/* a global variable, declared outside main() or any other function */

static int globsum = 20;

int main(void)
{
   int locsum = 0; // local variable in the main() function

   for (int i; i < 10; i++) // i is a local variable whithin the loop
   {
      printf("%d\n", i);
   }

   printf("%d\n", i);

   locsum = sum(10, 5);    // use of the local variable
   globsum = sum(10, 5);   // use of the local variable
   sumresult = sum(10, 5); // use of the local variable

   return 0;
}
