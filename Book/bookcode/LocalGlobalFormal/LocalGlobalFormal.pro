TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.c \
        sum.c

HEADERS += \
   sum.h

DISTFILES += \
   out.txt
