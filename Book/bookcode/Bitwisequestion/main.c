#include <stdio.h>

int main(void)
{
   unsigned char b1 = 0b00011010;
   unsigned char b2 = 0b00010000;
   unsigned char result;

   printf("b1 decimal: %d\n", b1);
   printf("b2 decimal: %d\n", b2);

   printf("b1 hex: 0x%02x\n", b1);
   printf("b2 hex: 0x%02x\n", b2);
   
   printf("b1 HEX: 0X%02X\n", b1);
   printf("b2 HEX: 0X%02X\n", b2);

   result = b1 & b2;
   printf("result after b1 & b2 operation: 0X%02X\n", result);

   result = b1 && b2;
   printf("result after b1 && b2 operation: 0X%02X\n", result);

   result = b1 >> 2;
   printf("result after b1 >> 2 operation: 0X%02X\n", result);

   result = b1 << 2;
   printf("result after b1 << 2 operation: 0X%02X\n", result);

   return 0;
}
