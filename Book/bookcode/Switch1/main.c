#include <stdio.h>

/* Demonstration of the switch-case statement */

int main(void)
{
   int motorToggle = 3; // value of the motortoggle

   switch (motorToggle)
   {
      case 0:
      {
         printf("motorToggle equals 0: switch motor off\n");
         break;
      }
      case 1:
      {
         printf("motorToggle equals 1: switch motor on\n");
         break;
      }
      case 2:
      {
         printf("motorToggle equals 2: motor to half power\n");
         break;
      }
      case 3:
      {
         printf("motorToggle equals 3: motor to full power\n");
         break;
      }
      case 4:
      {
         printf("motorToggle equals 4: switch motor brake on\n");
         break;
      }
      default:
      {
         printf("ERROR: motorToggle equals %d, this value is not defined\n",
                motorToggle);
      }
   }

   return 0;
}
