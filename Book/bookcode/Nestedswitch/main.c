#include <stdio.h>

int main(void)
{
   int motor = 1;    // Motor switch
   int brake = 1;    // Motor brake;
   int throttle = 3; // Motor throttle

   switch (motor)
   {
      case 0:
         printf("Motor off\n");
         switch (brake)
         {
            case 0:
               printf("Brake is off\n");
               break;
            case 1:
               printf("Brake is on\n");
               break;
         }
         break;
      case 1:
         printf("Motor on\n");
         switch (throttle)
         {
            case 0:
               printf("No throttle\n");
               switch (brake)
               {
                  case 0:
                     printf("Brake is off\n");
                     break;
                  case 1:
                     printf("Brake is still on\n");
                     break;
               }
               break;
            case 1:
               printf("Throttle at 1\n");
               switch (brake)
               {
                  case 0:
                     printf("Brake is off\n");
                     break;
                  case 1:
                     printf("Brake is still on\n");
                     break;
               }
               break;
            case 2:
            case 3:
               printf("Throttle at 2 or 3\n");
               switch (brake)
               {
                  case 0:
                     printf("Brake is off\n");
                     break;
                  case 1:
                     printf("Brake is still on\n");
                     break;
               }
               break;
         }
         switch (brake)
         {
            case 0:
               printf("Brake is off\n");
               break;
            case 1:
               printf("Brake is on\n");
               break;
         }
         break;
   }
   
   return 0;
}
