#include <stdio.h>

/* Show the conversion of Celsius to Fahrenheit in a table */

int main(void)
{
   int celsius = 0;
   double fahrenheit = 0.0;
   double celsius_rev = 0.0;

   printf("T in C\tT in F\tT to C reverted\n");


   /* fill out the the right code at the place of the ?? and ....*/

   for (celsius = 0; celsius <= 30; celsius = celsius + 5)
   {
      fahrenheit = (celsius * 1.8 ) + 32;
      celsius_rev = (fahrenheit - 32) / 1.8;
      printf("%4d\t%5.2lf\t%5.2lf\n", celsius, fahrenheit, celsius_rev);
   }

   return 0;
}
