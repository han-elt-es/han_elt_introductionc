#include <stdio.h>
#include <string.h>

typedef struct
{
   char name[36];
   int number;
   double value;
}  sensor_t;

void changeInt(int *ivar);
void changeStruct(sensor_t *fsensor);
void changeStructArray(sensor_t fsensors[3]);
void changeStructArrayString(sensor_t fsensors[3]);

int main(void)
{
   sensor_t sensor = {"temperatuur", 25, 1.123};
   printf("Call by reference using reference to struct sensor:\n");
   printf("name before changeStruct(&sensor): %s\n",
          sensor.name);
   printf("number before changeStruct(&sensor): %d\n",
          sensor.number);
   printf("value before changeStruct(&sensor): %lf\n",
          sensor.value);
   changeStruct(&sensor);
   printf("name after changeStruct(&sensor): %s\n",
          sensor.name);
   printf("number after changeStruct(&sensor): %d\n",
          sensor.number);
   printf("value after changeStruct(&sensor): %lf\n",
          sensor.value);
   printf("\n");

   sensor_t sensors[3] = {{"temperatuur", 25, 1.12},
                          {"snelheid", 2, 56.78},
                          {"rpm", 3, 17.8}};
   printf("Call by reference using reference to struct sensor:\n");
   printf("sensors[1].name before changeStruct(&sensor[1]): %s\n",
          sensors[1].name);
   printf("sensors[1].number before changeStruct(&sensor[1]): %d\n",
          sensors[1].number);
   printf("sensors[1].value before changeStruct(&sensor[1]): %lf\n",
          sensors[1].value);
   changeStruct(&sensors[1]);
   printf("sensors[1].name after changeStruct(&sensor[1]): %s\n",
          sensors[1].name);
   printf("sensors[1].number after changeStruct(&sensor[1]): %d\n",
          sensors[1].number);
   printf("sensors[1].value after changeStruct(&sensor[1]): %lf\n",
          sensors[1].value);
   printf("\n");

   printf("Call by reference using reference to struct sensor:\n");
   printf("name before changeStructArray(sensors): %s\n",
          sensors[1].name);
   printf("number before changeStructArray(sensors): %d\n",
          sensors[1].number);
   printf("value before changeStructArray(sensors): %lf\n",
          sensors[1].value);
   changeStructArray(sensors);
   printf("name after changeStructArray(sensors): %s\n",
          sensors[1].name);
   printf("number after changeStructArray(sensors): %d\n",
          sensors[1].number);
   printf("value after changeStructArray(sensors): %lf\n",
          sensors[1].value);
   printf("\n");

   printf("Call by reference using reference to struct sensor,\n"
          " changing 1 character:\n");
   printf("name before changeStructArray(sensors): %s\n",
          sensors[1].name);
   changeStructArrayString(sensors);
   printf("name after changeStructArray(sensors): %s\n",
          sensors[1].name);
   printf("\n");

   return 0;
}

void changeInt(int *ivar)
{
   *ivar = 15;
   return;
}

void changeStruct(sensor_t *fsensor)
{
   strcpy(fsensor->name, "tempsensor");
   fsensor->number = 10;
   fsensor->value = 1442.778;
   return;
}

void changeStructArray(sensor_t fsensors[3])
{
   strcpy(fsensors[1].name, "termovalve");
   fsensors[1].number = 33;
   fsensors[1].value = 145.1;
   return;
}

void changeStructArrayString(sensor_t fsensors[3])
{
   fsensors[1].name[3] = 'Q';
   return;
}
