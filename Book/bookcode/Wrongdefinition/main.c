#include <stdio.h>

/* Example of a wrong declaration and initialization */

int main(void)
{
   int i = 500;
   double c = 10.5;
   int d;
   // some code
   int i = 200; // Wrong! integer i is already defined

   return 0;
}
