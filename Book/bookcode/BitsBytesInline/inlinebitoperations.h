#ifndef INLINEBITOPERATIONS_H
#define INLINEBITOPERATIONS_H
// these are inline functions to do bitwise operations
// the variables for each function are the same
// "var" is the byte the operation manipulates
// "bit" is the bitnumber counting from right to left starting with 0
// the result is the byte as a result of the operation
// a byte is defined as an "unsigned character"

inline unsigned char setBit(unsigned char var, int bit);
inline unsigned char clearBit(unsigned char var, int bit);
inline unsigned char toggleBit(unsigned char var, int bit);
inline unsigned char testBitSet(unsigned char var, int bit);
inline unsigned char testBitclear(unsigned char var, int bit);
inline unsigned char setMask(unsigned char var, unsigned char mask);
inline unsigned char clearMask(unsigned char var, unsigned char mask);

#endif // INLINEBITOPERATIONS_H
