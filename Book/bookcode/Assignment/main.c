#include <stdio.h>

int main(void)
{
   // Examples of correct definitions, initializations and assignments
   int i = 500; 
   double c = 10.5;

   int d; // Not initialised, content of d is not defined!

   .....       // Some code
   i = 200;    // Correct! An already defined integer i
               // is assigned a new value

   return 0;
}
