/* Escape characters in strings */

#include <stdio.h>

int main(void)
{
   printf("Hello World\n");
   printf("\tHello\n\tC programmer\n");
   printf("\n");

   printf("h1\th2\n");
   printf("5\t3\n");
   printf("6\t7\n");

   printf("This is very 'nice'\n\n");

   printf(
      "More text using \\backslashes\\ and "
      "\"double quotes\"\n");

   return 0;
}
