#include <stdio.h>

int main(void)
{
   int motorspeed = 10;

   while (motorspeed < 10)
   {
      printf("[while loop] motor speed is %d speeding up motor\n",
             motorspeed);
      motorspeed = motorspeed + 2;
   }
   printf("[after while loop] motor speed is %d\n\n", motorspeed);

   motorspeed = 10;
   do
   {
      printf("[do while loop] motor speed is %d speeding up motor\n",
             motorspeed);
      motorspeed = motorspeed + 2;
   } while (motorspeed < 10);
   printf("[do do while loop] motor speed is %d\n", motorspeed);

   return 0;
}
