#include <stdio.h>

// Reference to an array with size of the array
void changeArray(int arrdata[], int size);
void printArray(const int arrdata[], int size);
void changeInt(int *pivar);

int main()
{
   int data1[3] = {10, 20, 30};
   printf("Call by reference using reference to variable '&':\n");
   printf("Changing just one element of data1 use: changeInt(&data1[1]\n");
   printf("Array data1 before change of data1[1]\n");
   printArray(data1, 3);
   changeInt(&data1[1]);
   printf("\nArray data1 after change of data1[1]\n");
   printArray(data1, 3);
   printf("\n\n");

   int data2[5] = {100, 200, 300, 400, 500};
   printf("Changing just one element of data2 use: changeInt(&data2[1]\n");
   printf("Array data1 before change of data2[1]\n");
   printArray(data2, 5);
   changeInt(&data2[1]);
   printf("\nArray data1 after change of data2[1]\n");
   printArray(data2, 5);
   printf("\n\n");

   printf("Call by reference using reference to array data1:\n");
   printf(
      "Increase all elements of data1 with 1 using changeArray(data1)\n");
   printf("Array data1 before change of data1\n");
   printArray(data1, 3);

   changeArray(data1, 3);
   printf("\nArray data1 after change of data1\n");
   printArray(data1, 3);
   printf("\n\n");

   return 0;
}

// changeArray() increases all the elements with 1
// the function has two formal parameters:
// int arrdata[]     point to the beginning of the array
// int size          gives the size in elements of the array
void changeArray(int arrdata[], int size)
{
   for (int i = 0; i < size; i++)
   {
      arrdata[i] = arrdata[i] + 1;
   }
}

void printArray(const int arrdata[], int size)
{
   for (int i = 0; i < size; i++)
   {
      printf("%d ", arrdata[i]);
   }
}

// changeInt() adds 15 to a given integer
// the function has one formal parameters:
// int *pivar     points to an integer, this a reference to the integer
void changeInt(int *pivar)
{
   // Use dereferencing '*' to change value pivar points to
   *pivar = *pivar + 15;
}
