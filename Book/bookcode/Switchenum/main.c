#include <stdio.h>

/* Demonstration of the switch-case statement with enumeration */

/* Enumeration of constants: enumerated data type
 *                0        1         2            3             4        */
enum command {MOTOR_OFF, MOTOR_ON, MOTOR_HALF, MOTOR_FULL, MOTOR_BRAKE_ON};

int main(void)
{
   enum command cmd = MOTOR_FULL;

   switch (cmd)
   {
      case MOTOR_OFF:
      {
         printf("cmd is 0 switch motor off\n");
         break;
      }
      case MOTOR_ON:
      {
         printf("cmd is 1 switch motor on\n");
         break;
      }
      case MOTOR_HALF:
      {
         printf("cmd is 2 motor to half power\n");
         break;
      }
      case MOTOR_FULL:
      {
         printf("cmd is 3 motor to full power\n");
         break;
      }
      case MOTOR_BRAKE_ON:
      {
         printf("cmd is 4 switch motor brake on\n");
         break;
      }
      default:
      {
         printf("ERROR: cmd is %d, this value is not defined\n", cmd);
      }
   }

   return 0;
}
