TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG = c11

SOURCES += \
   main.c \
   calculator.c

HEADERS += \
   calculator.h \
   AppInfo.h
