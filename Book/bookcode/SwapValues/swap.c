/* swap the values of a and b */
#include "swap.h"

void swapval(int a, int b)
{
   int t = a;

   a = b;
   b = t;
}

void swapref(int *pa, int *pb)
{
   int t = *pa;

   *pa = *pb;
   *pb = t;
}
