# Introduction C programming

Code examples:

All code examples can be compiled by QtCreator development environment:

- Select the related *.pro* file
- Click the *left green button* for compiling and running the code

For the writer:

Please use QT to add code examples.
If you want to add the screen results off a program add an out.txt file and copy the screenresult in it.

In latex the following sequence can be used to add the code to the book:
```
\lstinputlisting[caption=Whiledowhile/\textit{main.c},<br/>
frame=single, label={lst:Enum}]{../CodeExamples/Whiledowhile/main.c}<br/>
Output:\\<br/>
\begin{tcolorbox}<br/>
	\verbinput{../CodeExamples/Whiledowhile/out.txt}<br/>
\end{tcolorbox}<br/>
```

