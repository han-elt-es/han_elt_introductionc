/* Conversion characters in printf format strings */

#include <stdio.h>

int main(void)
{
   /* integer value */
   printf("Counted number of eggs = %d\n", 123);

   /* floating point double value */
   printf("Every egg cost %lf\n", 0.25);
   printf("Every egg cost %.2lf\n", 0.25);
   printf("Every egg cost %03.2lf\n", 0.25);

   return 0;
}
