#include <stdio.h>

int main(void)
{
   int a = 7;

   /* nesting if statements */

   if (a < 8)
   {
      if (a > 5)
      {
         if (a != 6)
         {
            printf("a must be 7, let's check this!\n");
            if (a == 7)
            {
               printf("Check, a is 7!\n");
            }
         }
      }
   }
   else
   {
      printf("This else belongs to a condition");
   }

   return 0;
}
