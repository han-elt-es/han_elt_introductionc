/* -----------------------------------------------------------------------------
 * This C program contains some compile time errors and several
 * run time errors (bugs). Solve these errors.
 *
 * C does not do any run time time checking for array boundaries.
 */

#include "StringProcessing.h"

#include <stdio.h>

#define MAXCHARS 10

int main(void)
{
   char text[MAXCHARS] = "abcdef12";

   puts("Program bugs2 started with testing\n");

   printf("countChar(\"abbccc\", 'a') = %d\n", countChar("abbccc", 'c'));
   printf("countChar(\"abbcccb\", 'b') = %d\n", countChar("abbcccb", 'b'));
   printf("countChar(\"bbccbb\", 'c') = %d\n", countChar("bbccbb", 'c'));
   printf("countChar(\"abbccc\", 'x') = %d\n\n", countChar("abbccc", 'x'));

   printf("String text = %s\n", text);
   reverseString(text);
   printf("reverseString(text) = %s\n\n", text);

   puts("\nProgram ready\n");

   return 0;
}
