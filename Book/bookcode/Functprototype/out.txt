I = 5.2 A
R = 32.0 Ohm
U = 166.4 V

P = 865.3 W

Result is more than 10 V

Current voltage is 10.0 V
Current voltage is 12.0 V
Current voltage is 14.0 V
Current voltage is 16.0 V
Current voltage is 18.0 V
Current voltage is 20.0 V
Current voltage is 22.0 V
Resulting voltage is 24.0 V
I = 1.0 A
R = 24.0 Ohm
U = 24.0 V

I = 5.0 A
R = 24.0 Ohm
U = 120.0 V

I = 1.0 A
R = 3.2 Ohm
U = 3.2 V

I = 5.3 A
R = 1.2 Ohm
U = 6.4 V

I = 0.0 A
R = 0.0 Ohm
U = 5.0 V
