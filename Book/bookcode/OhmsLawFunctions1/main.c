/* Ohm's law calculations in functions */

#include <stdio.h>

/* Prototypes of the calculation functions used in this program */
double calcVoltage(double I, double R);
double calcCurrent(double U, double R);
double calcResistance(double U, double I);

int main(void)
{
   /* Defining declarations, double data type and initialisations */
   double U1 = 0.0;
   double I1 = 5.2;
   double R1 = 32.0;

   /* Some function calls with different parameters */
   U1 = calcVoltage(I1, R1);
   /* Print all values: U1, I1 and R1 */
   printf("U1 = %.1lf V\n", U1);
   printf("I1 = %.1lf A\n", I1);
   printf("R1 = %.1lf Ohm\n\n", R1);

   double U2 = 24.0;
   double R2 = 5.0;
   double I2 = calcCurrent(U2, R2);
   /* Print all values: U2, I2 and R2 */
   printf("U2 = %.1lf V\n", U2);
   printf("I2 = %.1lf A\n", I2);
   printf("R2 = %.1lf Ohm\n\n", R2);

   U2 = 5.0;
   I2 = 0.001;
   R2 = calcResistance(U2, I2);
   /* Print all values: U2, I2 and R2 */
   printf("U2 = %.1lf V\n", U2);
   printf("I2 = %.1lf A\n", I2);
   printf("R2 = %.1lf Ohm\n", R2);

   return 0;
}

/* Definition of the calculation functions */
double calcVoltage(double I, double R)
{
   return I * R;
}

double calcCurrent(double U, double R)
{
   return U / R;
}

double calcResistance(double U, double I)
{
   return U / I;
}
