#include <stdio.h>

/* Using an if statement */
int main(void)
{
   int a = 5;

   if (a < 6)
   {
      printf("a is smaller than 6\n");
   }
   if (a > 6)
   {
      printf("a is greater than 6\n");
   }
   
   return 0;
}
