#include <stdio.h>

/* Demonstrates a simple do while statement */

int main(void)
{
   int motorspeed = 0;

   do
   {
      printf("Speeding up motor, current speed is: %d\n", motorspeed);
      motorspeed = motorspeed + 4; // simulate increasing speed
      printf("Speeding up motor, new speed is: %d\n", motorspeed);
   } while (motorspeed < 15);
   
   printf("Final motor speed is: %d\n", motorspeed);

   return 0;
}
