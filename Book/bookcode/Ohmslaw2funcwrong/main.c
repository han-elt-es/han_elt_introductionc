/* Ohm's law calculations */

#include <stdio.h>

/* Function calcVoltage:
 *    calculates and returns the voltage
 *    the function takes two parameters (values) current I and resitance R */

int calcVoltage(double I, int R)
{
  double U;
  U = I * R;
  return U;
}

int main(void)
{
   /* Defining declarations, double data type and initialisations */
   double U = 0.0;
   double I = 5.2;
   double R = 32.0;

   /* Calculate U using the calcvoltage function */
   U = calcVoltage(I,R);

   /* Print all values: U, I and R */
   printf("I = %.1lf A\n", I);
   printf("R = %.1lf Ohm\n", R);
   printf("U = %.1lf V\n\n", U);

   U = 0.0;
   I = 15.2;
   R = 32.0;

   /* Calculate U using the calcvoltage function */
   U = calcVoltage(I,R);

   /* Print all values: U, I and R */
   printf("I = %.1lf A\n", I);
   printf("R = %.1lf Ohm\n", R);
   printf("U = %.1lf V\n\n", U);

   U = 0.0;
   I = 20.4;
   R = 32.0;

   /* Calculate U using the calcvoltage function */
   U = calcVoltage(I,R);

   /* Print all values: U, I and R */
   printf("I = %.1lf A\n", I);
   printf("R = %.1lf Ohm\n", R);
   printf("U = %.1lf V\n\n", U);

   return 0;
}
