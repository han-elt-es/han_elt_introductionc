/* Print bits and bytes functions */
#ifndef PRINTBYTES_H
#define PRINTBYTES_H

// val2str transfers a unsinged integer value to a string of bytes
// byte[] points to a buffer of characters with the size 9
// val is the value to be converted to bits

void val2str(char byte[], unsigned char val, int lengthByte);
void printByte(unsigned char val, char text[]);
#endif
