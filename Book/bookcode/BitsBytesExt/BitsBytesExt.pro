TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        bytebitfunc.c \
        main.c

HEADERS += \
   bytebitfunc.h

DISTFILES += \
   out.txt
