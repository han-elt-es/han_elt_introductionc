#include <stdio.h>

#define SIZE 5

int main(void)
{
   int data[SIZE] = {2, 3, -10, 5, -20};
   int index = -1;

   for (int i = 0; i < SIZE; i++)
   {
      // Test for negative value
      if (data[i] < 0)
      {
         index = i;
         break;
      }
   }

   if (index != -1)
   {
      printf("Index first negative value in data array = %d value = %d\n",
             index, data[index]);
   }
   else
   {
      printf("No negative value found\n");
   }

   return 0;
}
