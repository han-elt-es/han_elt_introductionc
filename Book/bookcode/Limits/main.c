/* Value ranges integral data types */

#include <limits.h>
#include <stdio.h>

int main(void)
{
   printf("bits in char    = %u\n", CHAR_BIT);
   printf("signed char     = %d ... %d\n", SCHAR_MIN, SCHAR_MAX);
   printf("unsigned char   = %d ... %d\n\n", 0, UCHAR_MAX);

   printf("bits in short   = %lu\n", sizeof(short) * 8);
   printf("short           = %d ... %d\n", SHRT_MIN, SHRT_MAX);
   printf("unsigned short  = %d ... %d\n\n", 0, USHRT_MAX);

   printf("bits in int     = %lu\n", sizeof(int) * 8);
   printf("int             = %d ... %d\n", INT_MIN, INT_MAX);
   printf("unsigned int    = %u ... %u\n\n", 0, UINT_MAX);

   printf("bits in long    = %lu\n", sizeof(long) * 8);
   printf("long            = %ld ... %ld\n", LONG_MIN, LONG_MAX);
   printf("unsigned long   = %lu ... %lu\n\n", 0UL, ULONG_MAX);

   return 0;
}
