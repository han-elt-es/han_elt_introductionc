TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.c \
        putstring.c

DISTFILES += \
   out.txt

HEADERS += \
   putstring.h
