#include "putstring.h"

#include <stdio.h>
#include <string.h>

int main(void)
{
   char text[] = "Hello";
   int a = 120;
   char buffer[30];

   /* Print text using printf */

   printf("Text: %s\n", text);

   /* Print text using putstring */

   putstring("Text: ");
   putstring(text);
   putchar('\n');

   /* Print integer using printf */

   printf("Integer: %d\n", a);

   /* Print integer using putstring or putchar */

   /* Add code here */

   return 0;
}
