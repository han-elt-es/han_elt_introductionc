# Introduction C programming repository

## Repository purpose

This repository contains the supporting sources and information for the courses:

*   Introduction Software Engineering
*   Introduction Software Engineering: Software Architecture
*   Information Technology A
    *   CPROG V/P-programming (period 1)
*   Information Technology B
    *   CPROG P-programming (period 2)
    
## Repository content

*   Book
    *   Introduction C Programming
    *   Code examples from the book
*   Exercise
    *   Book Lab Exercises
    *   Sources lab Exercises
    Toolchain 
    *   Installation QtCreator
    *   Supporting files for the Qt Integrated Development Environment (IDE)
    *   Installation VisualStudioCode and PlantUML
    *   Codestyle guide
     
*   Statemachines
    *   ColaVendingMachine (CVM-1, CVM-2 and CVM-3)
        *   Documentation Cola Vending Machine (in doc dir)
        *   Sources Cola Vending Machine (in app dir)
        *   UML plantUML sources (in uml dir)
        *   Supporting files
    *   Statemachines UML Tutorial
        *   Turnstyle UML documentation
  ---

Maintenance and support:

*Han University of Applied Science*\
*Academy of Engineering and Automotive*\
*Embedded Systems Engineering*

Jos Onokiewicz\
John van den Hooven
